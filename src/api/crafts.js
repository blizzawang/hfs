import request from '@/utils/request'

export default {
  getPageList(page, limit,keyword) {
    return request({
      url: `/crafts/list/${page}/${limit}?keyword=${keyword}`,
      method: 'get',
    })
  },
  changeCraftsStatus(id,status) {
    return request({
      url: `/crafts/status/${id}/${status}`,
      method: 'get',
    })
  },
}
